import { ref, Ref } from 'vue'

type TypeHighlightClass = 'active' | '' | 'devui-tree_isDisabledNode'

type TypeUseHighlightNode = () => {
  currentHighLightNodeKey: Ref<string>
  highLightClick: (index: string) => void
  initHighLightNode: (isDisabled: boolean, key: string) => void,
  getHighLightClass: (key: string) => TypeHighlightClass
}

const HIGHLIGHT_CLASS = 'active'
const IS_DISABLED_FLAG = 'devui-tree_isDisabledNode'
const disabledNodeSet = new Set<string>()

const useHighlightNode: TypeUseHighlightNode = () => {

  const currentHighLightNodeKey = ref<string>()

  const highLightClick = (key: string): void => {
    if (disabledNodeSet.has(key)) return
    currentHighLightNodeKey.value = key
  }

  const initHighLightNode = (isDisabled: boolean, key: string): void => {
    if (isDisabled) disabledNodeSet.add(key)
  }

  const getHighLightClass = ((key: string): TypeHighlightClass => {
    if (disabledNodeSet.has(key)) return IS_DISABLED_FLAG
    if (key === currentHighLightNodeKey.value) return HIGHLIGHT_CLASS
    return ''
  })

  return {
    currentHighLightNodeKey,
    highLightClick,
    initHighLightNode,
    getHighLightClass
  }
}
export default useHighlightNode
